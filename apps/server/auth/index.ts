import { rpcStart } from "./rpc";
import { httpStart } from "./http";

rpcStart();
httpStart();
